<?php

use App\Currency;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {return view('welcome');});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/currencies', function (){
  $currencies = Currency::paginate(10);

  return view('currencies', compact('currencies'));
});


Route::get('/currency/{name}', function ($name){
  $currencies = Currency::where('name', '=', $name)->get();

  return view('currency', compact('currencies'));
})->where(['name' => '[a-zA-Z]{3}']);

function test($url) {
  try {
    $client = new GuzzleHttp\Client();
    $user = auth()->user();

    $response = $client->request('GET', $url, [
      'headers' => [
        'Authorization' => 'Bearer ' . $user['api_token'],
        'Accept'        => 'application/json',
      ],
    ]);

    dd(json_decode($response->getBody()));
  } catch (RequestException $exception) {
    dd($exception);
  }
}

Route::get('/test-api/currencies', function () {
  test('http://currencies.loc/api/currencies?page=2');
})->middleware('auth');

Route::get('/test-api/currency/{name}', function ($name) {
  test('http://currencies.loc/api/currency/'.$name);
})->middleware('auth')->where(['name' => '[a-zA-Z]{3}']);
