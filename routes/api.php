<?php

use App\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:api', 'throttle:1500,1'])->group(function (){
  Route::get('/currencies', function (Request $request) {
    $currencies = Currency::paginate(10);

    return $currencies->toJson();
  });

  Route::get('/currency/{name}', function ($name) {
    $currencies = Currency::where('name', '=', $name)->get();

    return $currencies->toJson();
  });
});

