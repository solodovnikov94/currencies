@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">{{ __('Dashboard') }}</div>

          <div class="card-body">

            @if(isset($currencies) && count($currencies) > 0)
              <ul>
                @foreach($currencies as $currency)
                  <li>
                    {{ $currency->name }}: {{ $currency->rate }}
                  </li>
                @endforeach
              </ul>
            @endif

            <div>
              {{ $currencies->links() }}
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
