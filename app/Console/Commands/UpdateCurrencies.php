<?php

namespace App\Console\Commands;

use App\Currency;
use Illuminate\Console\Command;

class UpdateCurrencies extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'currencies:update';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    try {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, 'https://api.privatbank.ua/p24api/exchange_rates?json&date=' . now()->format('d.m.Y'));
      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
      $request = json_decode(curl_exec($curl_handle));
      curl_close($curl_handle);

      if ($request && isset($request->exchangeRate) && isset($request->date)) {
        foreach ($request->exchangeRate as $key => $rate) {
          if ($key === 0) continue;

          if ($currency = Currency::where([['name', '=', $rate->currency], ['date', '=', $request->date]])->first()) {
            $currency->rate = $rate->purchaseRateNB;
            $currency->save();
          } else {
            $new_currency = new Currency();
            $new_currency->name = $rate->currency;
            $new_currency->rate = $rate->purchaseRateNB;
            $new_currency->date = $request->date;
            $new_currency->save();
          }
        }
      }

      echo 'Currencies updated';
    } catch (\Exception $e) {
      echo "Exception: ", $e->getMessage(), "\n";
    }

    return 0;
  }
}
